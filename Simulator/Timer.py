from threading import Timer

class LoopTimer(object):
    def __init__(self, interval, function):
        self._timer = None
        self.timeInterval = interval
        self.function = function
        self.running = False
        self.start()

    def _run(self):
        self.running = False
        self.start()
        self.function()

    def start(self):
        if not self.running:
            self._timer = Timer(self.timeInterval, self._run)
            self._timer.start()
            self.running = True

    def stop(self):
        self._timer.cancel()
        self.running = False