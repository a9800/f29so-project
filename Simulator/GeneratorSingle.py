'''
Created on 5 Mar 2020

@author: callu_000
'''
import random
import datetime
from pymongo import MongoClient
client = MongoClient("mongodb+srv://ctj1:Cdv2FaRFCgJOsc78@bcw-mcm5b.mongodb.net/test?retryWrites=true&w=majority")
db=client.BCWSmartHomeApp
                
def valueChange(maxValue,changeValue,floor,ceiling):
    i = random.randint(1,maxValue)
    if i > maxValue-1 and changeValue < ceiling:
        changeValue = changeValue + 1
    elif i < 2 and changeValue > floor:
        changeValue = changeValue - 1
    return changeValue

def roomReading(room):
    global d
    
    # Calls the valueChange function for each value
    temperature = valueChange(50,room["roomTemperature"],14,41)    
    humidity = valueChange(50,room["roomHumidity"],34,56)   
    carbonDioxideLevels = valueChange(5,room["roomCO2"],249,1101)    
    totalPower = 0
   
    # Iterates through every device, that has a key listed in this room
    for x in room["devices"]:
        for device in d:
            if device['_id'] == x:
                # Generates a random number from 1 to 1000, changing the device from on to off or vice versa if the value is 1000
                i = random.randint(1,1000)
                # Determines if the device is turned on, as this affects how its power value is to be altered
                if device["switchedOn"]: 
                    if i==1000:
                        device["switchedOn"] = False
                        device["powerUsage"] = 0
                    else:                        
                        results = valueChange(5,device["powerUsage"],49,801)
                        totalPower += results
                        device['powerUsage'] = results
                else:
                    if i==1000:
                        device["switchedOn"] = True
                        newVal = random.randint(50,200)
                        totalPower += newVal
                        device['powerUsage'] = newVal
        
    # Writes the new values
    room['roomTemperature'] = temperature
    room['roomHumidity'] = humidity
    room['roomCO2'] = carbonDioxideLevels
    room['power'] = totalPower
    

def updateHouse():
    global r
    global g
    global time
        
    avgTemp = 0
    avgHumid = 0
    avgCO2 = 0
    calculatedPower = 0
    roomCount = 0
    
    # Iterates through each room with the current homeID
    for room in r:
        results = roomReading(room)
        avgTemp += room['roomTemperature']
        avgHumid += room['roomHumidity']
        avgCO2 += room['roomCO2']
        calculatedPower -= room['power']
        roomCount += 1
        
    # Iterates through each generator with the current homeID
    for generator in g:
        if not (time.replace(hour=6, minute=30, second=0) <= time < time.replace(hour=18, minute=30)) and generator["deviceType"] == "Solar Panel":
            generator['powerGeneration'] = 0
        elif (time.replace(hour=11, minute=15) <= time < time.replace(hour=13, minute=45)) and generator["deviceType"] == "Solar Panel":
            if generator["powerGeneration"] < 350:
                newVal = random.randint(350,665)
            else:
                newVal = valueChange(20,generator["powerGeneration"],349,666)
            generator['powerGeneration'] = newVal
            calculatedPower += newVal
        else:
            if generator["powerGeneration"] == 0 or generator["powerGeneration"] > 515:
                newVal = random.randint(200,515)
            else:
                newVal = valueChange(20,generator["powerGeneration"],199,516)
            generator['powerGeneration'] = newVal
            calculatedPower += newVal
        
    # If the current time is found to be a whole hour (to the second), then a record is inserted in the records collection, which lists the current home values
    if time == time.replace(minute=0, second=0):
        # Counts the number of rooms with the current homeID, and finds the average values for all rooms
        avgTemp = round(avgTemp/roomCount)
        avgHumid = round(avgHumid/roomCount)
        avgCO2 = round(avgCO2/roomCount)
        db.records.update_one({"date": time},{"$set":{"temperature": avgTemp, "humidity": avgHumid, "CO2Levels": avgCO2, "power": calculatedPower}},True)
        # If the time is midnight, then a check is performed to see if there are records spanning back one week
        if time == time.replace(hour=0) and db.records.count_documents({"date": (time + datetime.timedelta(days=-7))})>0:
            # If this is true, then the average for each record values is calculated and added to the manager records
            avgTemp = 0
            avgHumid = 0
            avgCO2 = 0
            calculatedPower = 0
            timeCopy = time + datetime.timedelta(days=-7)
            
            # Iterates through each past record in the time span for this week
            for x in range(169):
                currentRecord = db.records.find_one({"date": timeCopy})
                avgTemp += currentRecord["temperature"]
                avgHumid += currentRecord["humidity"]
                avgCO2 += currentRecord["CO2Levels"]
                calculatedPower += currentRecord["power"]
                timeCopy += datetime.timedelta(hours=1)
            
            # Finds the average of each value and writes it to the manager records, along with the start and end date for the week    
            avgTemp = round(avgTemp/169)
            avgHumid = round(avgHumid/169)
            avgCO2 = round(avgCO2/169)
            calculatedPower = round(calculatedPower/169)
            db.managerrecords.update_one({"weekStart": (time + datetime.timedelta(days=-7)), "weekEnd": time},{"$set":{"temperature": avgTemp, "humidity": avgHumid, "CO2Levels": avgCO2, "power": calculatedPower}},True)
        
secondsToRun = (int(input("Please enter the number of days you wish to create values for"))*86400)+1

# Takes the desired date from the user
time = datetime.datetime(int(input("Please enter the year you wish to generate data for")),int(input("Please enter the number of the month you wish to generate data for")),int(input("Please enter the day of the month you wish to generate data for")),0,0,0,0)

# Creates blank lists to hold each of the different collections
r = []
d = []
g = []

# Iterates through every collection and stores a copy of their values in the predefined lists
for room in db.rooms.find():
    r.append(room)
    
for device in db.devices.find():
    d.append(device)   
    
for generator in db.gendevices.find():
    g.append(generator)

# Iterates for each second in a day (86400 seconds), and calls the update house function each time
for x in range(secondsToRun):
    # Replaces time to be equal to the current counted second
    updateHouse()
    time += datetime.timedelta(seconds=1)

print("Values generated")