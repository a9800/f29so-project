'''
Created on 15 Jan 2020

@author: callu_000 a9800
'''
import random
#from time import sleep
import datetime
from Timer import LoopTimer
from pymongo import MongoClient
client = MongoClient("mongodb+srv://ctj1:Cdv2FaRFCgJOsc78@bcw-mcm5b.mongodb.net/test?retryWrites=true&w=majority")
db=client.BCWSmartHomeApp
                
def valueChange(maxValue,changeValue,statement,floor,ceiling):
    i = random.randint(1,maxValue)
    changed = False
    if i > maxValue-1 and changeValue < ceiling:
        changeValue = changeValue + 1
        changed = True
        print(statement % changeValue)
    elif i < 2 and changeValue > floor:
        changeValue = changeValue - 1
        changed = True
        print(statement % changeValue)
    return [changeValue,changed]

def roomReading(room):
    
    # Calls the valueChange function for each value
    results = valueChange(50,room["roomTemperature"],room["roomName"] + " Temperature = %s Degrees",14,41)
    temperature = results[0]
    changeCheck = results[1]   
    
    results = valueChange(50,room["roomHumidity"],room["roomName"] + " Humidity = %s Percent",34,56)
    humidity = results[0]
    if results[1]: changeCheck = True     
    
    results = valueChange(5,room["roomCO2"],room["roomName"] + " Carbon dioxide reading = %s ppm",249,1101)
    carbonDioxideLevels = results[0]
    if results[1]: changeCheck = True        
    
    totalPower = 0
    
    # Iterates through every device, that has a key listed in this room
    for x in room["devices"]:
        device = db.devices.find_one({"_id": x},{ "switchedOn": 1, "powerUsage": 1, "_id": 0 })
        # Determines if the device is turned on, as this affects how its power value is to be altered
        if device["switchedOn"]: 
            if device["powerUsage"]==0:
                newVal = random.randint(50,200)
                totalPower += newVal
                db.devices.update_one({"_id": x},{"$set":{"powerUsage": newVal}})
            else:
                results = valueChange(5,device["powerUsage"],room["roomName"] + " - Device Key: " + str(x) + " = %s watts",49,801)
                if results[1]: 
                    db.devices.update_one({"_id": x},{"$set":{"powerUsage": results[0]}})
                totalPower += results[0]
        elif device["powerUsage"]>0:
            db.devices.update_one({"_id": x},{"$set":{"powerUsage": 0}})
        
    # Writes the new values to the database
    if changeCheck: db.rooms.update_one({"roomName": room["roomName"]},{"$set": {"roomTemperature": temperature, "roomHumidity": humidity, "roomCO2": carbonDioxideLevels}})
    db.rooms.update_one({"roomName": room["roomName"]},{"$set": {"power": totalPower}})
    # Returns the read and (perhaps) changed values, along with the total power
    return [temperature,humidity,carbonDioxideLevels,totalPower]
    

def updateHouse():
    # Obtains the current time to the second
    now = datetime.datetime.now().replace(microsecond = 0)
    # Alters the obtained time to equal an exact hour
    recordTime = now.replace(minute=0, second=0)

    avgTemp = 0
    avgHumid = 0
    avgCO2 = 0
    calculatedPower = 0
    
    # Iterates through each room
    for room in db.rooms.find({}):
        results = roomReading(room)
        avgTemp += results[0]
        avgHumid += results[1]
        avgCO2 += results[2]
        calculatedPower -= results[3]
        
    # Iterates through each generator
    for generator in db.gendevices.find({}):
        if not (recordTime.replace(hour=6, minute=30) <= now < recordTime.replace(hour=18, minute=30)) and generator["deviceType"] == "Solar Panel":
            db.gendevices.update_one({"_id": generator["_id"]},{"$set":{"powerGeneration": 0}})
        elif (recordTime.replace(hour=11, minute=15) <= now < recordTime.replace(hour=13, minute=45)) and generator["deviceType"] == "Solar Panel":
            if generator["powerGeneration"] < 350:
                newVal = random.randint(350,665)
                db.gendevices.update_one({"_id": generator["_id"]},{"$set":{"powerGeneration": newVal}})
            else:
                results = valueChange(20,generator["powerGeneration"],generator["deviceName"] + " - Generator Key: " + str(generator["_id"]) + " = %s watts",349,666)
                newVal = results[0]
                if results[1]: 
                    db.gendevices.update_one({"_id": generator["_id"]},{"$set":{"powerGeneration": newVal}})
            calculatedPower += newVal
        else:
            if generator["powerGeneration"] == 0 or generator["powerGeneration"] > 515:
                newVal = random.randint(200,515)
                db.gendevices.update_one({"_id": generator["_id"]},{"$set":{"powerGeneration": newVal}})
            else:
                results = valueChange(20,generator["powerGeneration"],generator["deviceName"] + " - Generator Key: " + str(generator["_id"]) + " = %s watts",199,516)
                newVal = results[0]
                if results[1]: 
                    db.gendevices.update_one({"_id": generator["_id"]},{"$set":{"powerGeneration": newVal}})
            calculatedPower += newVal
    
    # Counts the number of rooms with the current homeID, and finds the average values for all rooms
    rooms = db.rooms.count_documents({})
    avgTemp = round(avgTemp/rooms)
    avgHumid = round(avgHumid/rooms)
    avgCO2 = round(avgCO2/rooms)
        
    db.homes.update_one({},{"$set":{"homeTemperature": avgTemp, "homeHumidity": avgHumid, "homeCO2": avgCO2, "homePower": calculatedPower}},True)
    # If the current time is found to be a whole hour (to the second), then a record is inserted in the records collection, which lists the current home values
    if now == recordTime:
        db.records.update_one({"date": now},{"$set":{"temperature": avgTemp, "humidity": avgHumid, "CO2Levels": avgCO2, "power": calculatedPower}},True)
        # If the time is midnight, then a check is performed to see if there are records spanning back one week
        if now == now.replace(hour=0) and db.records.count_documents({"date": (now + datetime.timedelta(days=-7))})>0:
            # If this is true, then the average for each record values is calculated and added to the manager records
            avgTemp = 0
            avgHumid = 0
            avgCO2 = 0
            calculatedPower = 0
            timeCopy = now + datetime.timedelta(days=-7)
            
            # Iterates through each past record in the time span for this week
            for x in range(169):
                currentRecord = db.records.find_one({"date": timeCopy})
                avgTemp += currentRecord["temperature"]
                avgHumid += currentRecord["humidity"]
                avgCO2 += currentRecord["CO2Levels"]
                calculatedPower += currentRecord["power"]
                timeCopy += datetime.timedelta(hours=1)
                
            # Finds the average of each value and writes it to the manager records, along with the start and end date for the week 
            avgTemp = round(avgTemp/169)
            avgHumid = round(avgHumid/169)
            avgCO2 = round(avgCO2/169)
            calculatedPower = round(calculatedPower/169)
            db.managerrecords.update_one({"weekStart": (now + datetime.timedelta(days=-7)), "weekEnd": now},{"$set":{"temperature": avgTemp, "humidity": avgHumid, "CO2Levels": avgCO2, "power": calculatedPower}},True)

# Sets the timer to call updateHouse every second
print("Starting timer...")
rt = LoopTimer(1, updateHouse)
# The below try, causes the program to stop after a set number of seconds. This was for testing purposes, and as such is commented out
'''
try:
    sleep(10) # Causes the timer to tick for the given number of seconds
finally:
    rt.stop()
'''