'''
Created on 23 Feb 2020

@author: callu_000
'''

import unittest
from Timer import LoopTimer
#from time import sleep
import datetime
from pymongo import MongoClient
client = MongoClient("mongodb+srv://ctj1:Cdv2FaRFCgJOsc78@bcw-mcm5b.mongodb.net/test?retryWrites=true&w=majority")
db=client.BCWSmartHomeApp

class TestDatabase(unittest.TestCase):
    
    def test_room_temperatures(self):
        for room in db.rooms.find():
            self.assertTrue(14 <= room['roomTemperature'] <= 41, "ERROR with " + room['roomName'] + "! Temperature should be between (including) 14 and 41")
    
    def test_room_humidity(self):
        for room in db.rooms.find():
            self.assertTrue(34 <= room['roomHumidity'] <= 56, "ERROR with " + room['roomName'] + "! Humidity should be between (including) 34 and 56")
            
    def test_room_CO2levels(self):
        for room in db.rooms.find():
            self.assertTrue(249 <= room['roomCO2'] <= 1101, "ERROR with " + room['roomName'] + "! CO2 should be between (including) 249 and 1101")
    
    def test_device_power(self):
        for device in db.devices.find():
            if device['switchedOn']:
                self.assertTrue(49 <= device['powerUsage'] <= 801, "ERROR with " + device['deviceName'] + "! Power should be between (including) 49 and 801")
            else:
                self.assertTrue(device['powerUsage'] == 0, "Should be 0")
                
    def test_generator_power(self):
        # Obtains the current time to the second
        now = datetime.datetime.now().replace(second=0, microsecond = 0)
        for generator in db.gendevices.find():
            if not (now.replace(hour=6, minute=30) <= now < now.replace(hour=18, minute=30)) and generator["deviceType"] == "Solar Panel":
                self.assertTrue(generator['powerGeneration'] == 0, "ERROR with " + generator['deviceName'] + "! Power should be 0")
            elif (now.replace(hour=11, minute=15) <= now < now.replace(hour=13, minute=45)) and generator["deviceType"] == "Solar Panel":
                self.assertTrue(349 <= generator['powerGeneration'] <= 666, "ERROR with " + generator['deviceName'] + "! Power should be between (including) 349 and 666")
            else:
                self.assertTrue(199 <= generator['powerGeneration'] <= 516, "ERROR with " + generator['deviceName'] + "! Power should be between (including) 199 and 516")
            
                
def runTest():
    if __name__ == "__main__":
        unittest.main()

print("Starting Tests")
rt = LoopTimer(1, runTest)

# The below try, causes the program to stop after a set number of seconds. Comment this out to run infinitely 
'''
try:
    sleep(10) # Causes the timer to tick for the given number of seconds
finally:
    rt.stop()
'''