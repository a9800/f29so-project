//requiring various packages and models needed 
//for the controllers logic to work
const User = require('../models/user');
const Room = require('../models/room');
const Record = require('../models/record');
const ManagerRecord = require('../models/managerrecord');
const Device = require('../models/device');
const GenDevice = require('../models/gendevice');
const passport = require('passport');
const util = require('util');

//exporting all controllers so they can be required by routes.
module.exports = {

  //INTITAL CONTROLLERS
  //GET landing page ... renders landing page view.
  getLanding(req, res, next) 
  {
    res.render('landing');
  },

  //GET terms page ... renders terms page view.
  getTerms(req, res, next) 
  {
    res.render('terms');
  },

  //GET home page ... renders home page view.
  async getHome(req, res, next) 
  {
    let rooms = await Room.find({});
    var powerUsage = 0;
    var temp = 0;
    var humidity = 0;
    var c02 = 0;
    var noRooms = 0;
    var power = 0;

    //looping through all rooms.
    //totals co2, powerusage, temperature, humidity.
    try 
    {
      for (const room of rooms) 
      {
        powerUsage += room.power;
        temp += room.roomTemperature;
        noRooms += 1
        humidity += room.roomHumidity;
        c02 += room.roomCO2;
      }

      //Getting average values for temp, humiditiy and c02
      var temp = Math.round(temp / noRooms);
      var humidity = Math.round(humidity / noRooms);
      var c02 = Math.round(c02 / noRooms);
      
      
      //need to check if humidity is greater than 100 if so set to 100. 
      if (humidity > 100)
       {
        var humidity = 100;
       }
      res.render('index', {temp, humidity, c02, powerUsage,  power, title: 'Home Page' })
    } catch (error) 
       {
       console.log(error);
       }
  },


  //GET stats page
  async getStats(req, res, next) 
  {
    let records = await Record.find({});
    try 
    {
      res.render('stats', { "data": JSON.stringify(records) });
    } catch (error) 
    {
      console.log(error);
    }

  },

  //ROOMS CONTROLLERS
  //GET rooms page ... renders all rooms.
  async getRooms(req, res, next) 
  {
    let rooms = await Room.find({});
    let user = await User.findById(req.user.id);
    res.render('room/index', { rooms, user, title: "Rooms" });
  },

  // GET rooms form ... renders the form for creating a new room
  async getRoomsForm(req, res, next) 
  {
    res.render('room/new', { title: 'Create Room' });
  },

  // POST new room ... allows a user or home manager to create a new room.
  async postNewRoom(req, res, next) 
  {
    const roomImage = req.body.roomImage;
    const roomName = req.body.roomName;
    const newRoom = {roomImage, roomName};
    Room.create(newRoom, function (err) 
    {
      if (err) 
      {
        console.log(err);
        req.flash("error", "Room already exists. Choose another name.");
        res.redirect('/room');
      } else
       {
        res.redirect('/room');
       }
    });
  },

 //SHOW room info ... renders room based on id.
  showRoomInfo(req, res, next) 
  {
    Room.findById(req.params.id).exec(function (err, foundRoom) {
      if (err) 
      {
        console.log(err);
      } else 
      {
        res.render('room/show', { foundRoom, title: "Room Info" });
      }
    });
  },

  //EDIT room... renders the form for editing a room
  async editRoom(req, res, next) 
  {
    let room = await Room.findById(req.params.id);
    res.render('room/edit', { room, title: "Edit Room" });
  },

  //UPDATE room ... sends off a post request to update the room
  async updateRoom(req, res, next) 
  {
    await Room.findByIdAndUpdate(req.params.id, req.body.room);
    req.flash("success", "Room updated!");
    res.redirect('/room');
  },

  //DESTROY room
  //Deletes the room and also all devices associated with it.
  //Cascade delete is used.
  destroyRoom(req, res, next) 
  {
    Room.findById(req.params.id, (err, room) => 
    {
      Device.remove(
      {
        "_id": 
        {
          $in: room.devices
        }
      }, (err) => 
      {
        if (err) return next(err);
        room.remove();
        req.flash("success", "Room deleted!");
        res.redirect('/room');
      });
    });
  },



  //DEVICES CONTROLLERS

  //GET device form ... renders a form for creating a new device
  //in a room
  async showDeviceForm(req, res, next) 
  {
    let room = await Room.findById(req.params.id);
    res.render('devices/new', { room, title: "Add Device" });
  },

  //SHOW devices...populates a room with devices based on a foundRoom id. 
  showDevices(req, res, next) 
  {
    let device = Device.find({});
    Room.findById(req.params.id).populate("devices").exec(function (err, foundRoom) 
    {
      if (err) 
      {
        console.log(err);
      } else 
      {
        res.render('devices/show', { foundRoom, title: "Devices" });
      }
      if (device.switchedOn === false) 
      {
        device.powerUsage.push(0);
      }
    });

  },

  //POST new device ... sends a post request to create a new device.
  postNewDevice(req, res, next) 
  {
    Room.findById(req.params.id, function (err, room) 
    {
      if (err) 
      {
        console.log(err);
        res.redirect('/room');
      } 
      else 
      {
        Device.create(req.body.device,function (err, device) 
        {
          if (err) 
          {
            console.log(err);
            req.flash("error", "Device already exists!");
            res.redirect('/room');
          } 
          else 
          {
            //save device
            device.save();
            room.devices.push(device);
            room.save();
            res.redirect('/room/' + room._id + '/devices');
          }
        });
      }
    });

  },

  //EDIT device ... renders a form for editing a device with
  //the existing device populated.
  async editDevice(req, res, next) 
  {
    let foundDevice = await Device.findById(req.params.device_id);
    let room = await Room.findById(req.params.id);
    res.render('devices/edit', { foundDevice, room, title: "Edit Device" });
  },

  //UPDATE device ... sends a post request to update the device
  async updateDevice(req, res, next) 
  {
    await Device.findByIdAndUpdate(req.params.device_id, req.body.device);
    req.flash("success", "Device Updated!");
    res.redirect('/room/' + req.params.id + '/devices');
  },

  //DESTROY device... deletes a device from the database. Both from the device collection
  //and array in the rooms collection.
  async destroyDevice(req, res, next) 
  {
    await Room.findByIdAndUpdate(req.params.id,
      {
        $pull: {devices: req.params.device_id}
      });
    await Device.findByIdAndRemove(req.params.device_id);
    req.flash("success", "Device deleted!");
    res.redirect('/room/' + req.params.id + '/devices');
  },


  //TURNDEVICEON...enables a device to be turned on or off. 
  async turnDeviceOn(req, res, next) 
  {
    let foundDevice = await Device.findByIdAndUpdate(req.params.device_id, req.body.switchedOn);
    res.render(foundDevice, '/room/' + req.params.id + '/devices');
  },



  //USER CONTROLLERS
  //GET /register ... renders the registration view.
  getRegister(req, res, next) 
  {
    res.render('register');
  },

  // POST /register ... sening a POST request to register 
  // a user, storing their details in the database.
  async postRegister(req, res, next) 
  {
    //new user object 
    const newUser = new User
    (
      {
      //trim() ensures if a user mistakenly leaves whitespace
      //it is removed before saving to the database.
      username: req.body.username.trim(),
      email: req.body.email.trim(),
      password: req.body.password,
      userFirstName: req.body.userFirstName.trim(),
      userLastName: req.body.userLastName.trim(),
      userAvatar: req.body.userAvatar.trim(),
      accounttype: req.body.accounttype,
      }
    );
    //creating user variable comprised of their details and password
    let user = await User.register(newUser, req.body.password);
    //passing user variable into login method.
    req.login(user, (err) => 
    {
      if (err) 
      {
        req.flash("error", "User already exists with this email!");
        return res.redirect('/register');
      }
      else 
      {
        req.flash("success", "You are now regsistered " + req.body.username);
        res.redirect('/room');
      }
    });
  },

  //GET /login ... renders the login view
  getLogin(req, res, next)
   {
    res.render('login');
  },

  //POST /login ... sends a POST request which uses passport js 
  //authenticate method to check a users username and password.
  //Upon success the home page is rendered and a success flash message
  //appears. Upon failure the login page is rendered again and a failure
  //flash message appears.
  postLogin(req, res, next)
   {
    passport.authenticate('local', 
    {
      successRedirect: '/',
      failureRedirect: '/login',
      failureFlash: "Sorry! Please try again.",
      successFlash: "You are now logged in " + req.body.username + "!"
    })(req, res, next);
  },

  // GET /logout ... provides logout functionality through passport js.
  //When the logout button is pressed the session is terminated and
  //the landing page view is rendered. 
  getLogout(req, res, next) 
  {
    req.logout();
    res.redirect('/landing');
  },

  //SHOW my details ... renders the current logged in users account 
  //details 
  async showMyDetails(req, res, next) 
  {
    let currentUser = await User.findById(req.params.id);
    res.render('account/show', { currentUser, title: "Account" });
  },

  //EDIT my details ... renders the edit account details page with
  //the users details pre populated in the form 
  async editMyDetails(req, res, next)
   {
    let currentUser = await User.findById(req.params.id);
    res.render('account/edit', { currentUser, title: "Edit Account" });
   },

  //UPDATE my details ... sends the form off via a post request to 
  //update the currently logged in users details. Also automatically
  //terminates the current session and logs the user in thus creating 
  //a new session and renders the home page with a success flash message.
  async updateMyDetails(req, res, next, err) 
  {
    const 
    {
      username,
      email,
      userAvatar,
      userFirstName,
      userLastName,
    } = req.body;

    const {user} = res.locals;
    if (username) user.username = username;
    if (email) user.email = email;
    if (userAvatar) user.userAvatar = userAvatar;
    if (userFirstName) user.userFirstName = userFirstName;
    if (userLastName) user.userLastName = userLastName;

    await user.save();
    const login = util.promisify(req.login.bind(req));
    await login(user);
    req.flash("success", "Profile successfully updated!");
    res.redirect('/room');
  },



  //HOME MANAGER CONTROLLERS

  //GET user details ... renders all registered users details.
  //Excludes password.
  async getUserDetails(req, res, next) 
  {
    let currentUser = await User.findById(req.user.id);
    let users = await User.find({});
    res.render('homemanager/index', { users, currentUser, title: 'Users' });
  },

  //EDIT user details ... renders a populated form of a particular
  //users details based on an associated id.
  async editUserDetails(req, res, next) 
  {
    let user = await User.findById(req.params.id);
    res.render('homemanager/edit', { user, title: "Users Edit" });
  },

  //UPDATE user details ... posts the update form and renders the home manager
  //page showing the newly updated user
  async updateUserDetails(req, res, next) 
  {
    await User.findByIdAndUpdate(req.params.id, req.body.user);
    req.flash("success", "User details updated!");
    res.redirect('/homemanager');
  },

  //DESTROY user details ... allows a home manager to delete a user.
  async deleteUserDetails(req, res, next)
   {
    await User.findByIdAndRemove(req.params.id);
    req.flash("success", "User deleted!");
    res.redirect('/homemanager');
  },

 
  //GEN DEVICES CONTROLLERS
  //renders generation devices.
  //creates a total power generation stat from generation devices.
  //creates a total power usage stat adding the power consumption from each room to a total.
  async getGenDevices(req, res, next)
   {
    let rooms = await Room.find({});
    let genDevices = await GenDevice.find({});
    var powerGeneration = 0;
    for (const genDevice of genDevices) 
    {
      powerGeneration += genDevice.powerGeneration;
    }
    var powerUsage = 0;
    for (const room of rooms) 
    {
      powerUsage += room.power;
    }
    res.render('gendevices/index', { genDevices, powerUsage, powerGeneration, title: 'Energy Generation Devices' });
   },

 //GET stats page...renders stats for the home manager. 
  async getHomeManagerStats(req, res, next) 
  {
  let managerrecords = await ManagerRecord.find({});
    res.render('homemanager/homemanagerstats', {managerrecords, "data": JSON.stringify(managerrecords) });
  },

}


