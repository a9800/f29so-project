//Generation Device model. Provides a structure for how a gendevice document
//should be structured in the gendevices collection in the database.
const mongoose = require('mongoose');
const GenDeviceSchema = new mongoose.Schema
(
    {
    deviceName: {type:String, unique:true},
    deviceImage: String,
    deviceType: String,
    powerGeneration: {type: Number, default: 0},
    }
);
module.exports = mongoose.model('GenDevice', GenDeviceSchema);