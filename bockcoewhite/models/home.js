//Home model. Provides a structure for how a home document
//should be structured in the homes collection in the database.
const mongoose = require('mongoose');
const HomeSchema = new mongoose.Schema
(
  {
    homeTemperature: {type:Number, default: 30},
    homeHumidity: {type:Number, default: 50},
    homeCO2: {type:Number, default: 350},
    homePower: {type:Number, default: 500},
  }
);
module.exports = mongoose.model('Home', HomeSchema);