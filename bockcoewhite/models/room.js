//Room model. Provides a structure for how a room document
//should be structured in the rooms collection in the database.
const mongoose = require('mongoose');
const RoomSchema = new mongoose.Schema
(
  {
    roomImage: String,
    roomName: {type:String, unique:true},
    roomTemperature: {type:Number, default: 30},
    roomHumidity: {type:Number, default: 50},
    roomCO2: {type:Number, default: 350},
    power: {type:Number, default: 500},
    devices: 
      [
        {
          type: mongoose.Schema.Types.ObjectId,
          ref: "Device"
        }
      ],
    }
  );
module.exports = mongoose.model('Room', RoomSchema);