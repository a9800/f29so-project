//Device model. Provides a structure for how a device document
//should be structured in the devices collection in the database.
const mongoose = require('mongoose');
const DeviceSchema = new mongoose.Schema
(
    {
    deviceName: {type:String, unique:true},
    deviceImage: String,
    switchedOn: {type: Boolean, default: false},
    powerUsage: {type: Number, default: 0},
    deviceType: String
    }
);
module.exports = mongoose.model('Device', DeviceSchema);