//user model. Provides a structure for how a user document
//should be structured in the users collection in the database.
const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');
const UserSchema = new mongoose.Schema
(
 {
    username: String,
    isHomeManager: {type:Boolean, default:false},
    accounttype: String,
    userAvatar: String,
    userFirstName: String,
    userLastName: String,
    email: {type: String, unique:true},
 }
);
UserSchema.plugin(passportLocalMongoose);
module.exports = mongoose.model('User', UserSchema);