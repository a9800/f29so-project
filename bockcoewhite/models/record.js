//Records model. Provides a structure for how a record document
//should be structured in the records collection in the database.
const mongoose = require('mongoose');
const RecordSchema = new mongoose.Schema
(
    {
    date: {type:Date},
    CO2Levels: {type:Number},
    humidity:{type:Number} ,
    power: {type:Number},
    temperature: {type:Number}
    }
);
module.exports = mongoose.model('Record', RecordSchema);