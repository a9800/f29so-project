//Manager Records model. Provides a structure for how a managerrecord document
//should be structured in the managerrecords collection in the database.
const mongoose = require('mongoose');
const ManagerRecordSchema = new mongoose.Schema
(
    {
    weekEnd: {type:Date},
    weekStart: {type:Date},
    CO2Levels: {type:Number},
    humidity:{type:Number} ,
    power: {type:Number},
    temperature: {type:Number}
    }
);
module.exports = mongoose.model('ManagerRecord', ManagerRecordSchema);