//Requiring models to be used in middleware
const User = require('../models/user');
const Room = require('../models/room');


module.exports = {
    //async error handler. All controllers which use async/await are passed 
    //through this error handler that catches any errors should an asynchronous call back fail.
    asyncErrorHandler: (fn) =>
        (req,res,next) =>
        {
            Promise.resolve(fn(req,res,next))
            .catch(next);
        },

        //ensures that the user is logged in before a session is created 
        //if they aren't logged in and they try and access a route
        // an error message appears.
        isLoggedIn: (req,res,next)=>
        {
            if(req.isAuthenticated())
            {
                return next();
            }
            req.flash("error", "Please Login First!");
            res.redirect("/login");
        },

        //ensures that only adults can access certain content. 
        isAdult: async (req,res,next)=>
        {
            if(req.isAuthenticated() && req.user.accounttype == "Adult")
            {
                return next();
            }
                res.render('unauthorized');
        },

        //ensures that only home managers can access certain content.
        isHomeManager: async (req,res,next)=>
        {
            if(req.isAuthenticated() && req.user.isHomeManager)
            {
                return next();
            }
                res.render('unauthorized');
            },

    
        //used exclusively in the user account update route
        //checks that the password being entered by the user
        // when updating their details is correct. It provides 
        // an extra level of security when a user
        //tries to update their details.
        isValidPassword: async (req,res,next)=>
        {
            const {user} = await User.authenticate()(req.user.username, req.body.currentPassword);
            if(user)
            {
                // add user to res.locals
                res.locals.user = user;
                next();
            }else{
                req.flash("error","Incorrect Password!");
                return res.redirect('/');
            }
        },

            //used exclusively in the user account update route
            //takes the new password and confirmation password 
            //from the body and first checks that both have been entered
            //if not an error is displayed. if both are present it 
            //sets the password to the new password. It also checks that 
            //the new and confirmation passwords match otherwise an 
            //error message appears.
            changePassword: async (req,res,next)=>
            {
             const
              {
                 newPassword,
                 confirmationPassword
              } = req.body;

             if(newPassword && !confirmationPassword)
             {
                 req.flash("error", "Missing password confirmation!");
                 return res.redirect('/');
             }
             else if(newPassword && confirmationPassword)
             {
                 const {user} = res.locals;
                 if(newPassword === confirmationPassword)
                 {
                     await user.setPassword(newPassword);
                     next();
                 }
                 else
                 {
                     req.flash("error",'New passwords must match!');
                     return res.redirect('/');
                 }
             }
             else
             { 
             next();
             }
            },
};


