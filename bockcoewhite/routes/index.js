
const express = require('express');
const router = express.Router();

//requiring all the controllers needed for 
//their matching routes. All Logic is abstracted 
//away from routes and resides in controllers.
const {
       getLanding,
       getTerms,
       getRegister,
       postRegister,
       getLogin,
       postLogin,
       getLogout,
       getRooms,
       getRoomsForm,
       postNewRoom,
       showRoomInfo,
       editRoom,
       updateRoom,
       destroyRoom,
       showMyDetails,
       editMyDetails,
       updateMyDetails,
       getUserDetails,
       editUserDetails,
       updateUserDetails,
       deleteUserDetails,
       showDevices,
       showDeviceForm,
       postNewDevice,
       destroyDevice,
       editDevice,
       updateDevice,
       turnDeviceOn,
       getStats,
       getHome,
       getGenDevices,
       getHomeManagerStats
      } = require('../controllers/index');


      //Requiring all middleware that provides validation 
      //and authorization checks on the various routes.
      const { 
       asyncErrorHandler, 
       isLoggedIn,
       isAdult,
       isHomeManager,
       isValidPassword,
       changePassword,
      } = require('../middleware');

//All of the HTTP routes required for the application reside here. 

//INITIAL ROUTES
/* GET landing page */
router.get('/landing', getLanding);

/* GET terms */
router.get('/terms', getTerms);


/* GET stats */
router.get('/stats',getStats);

/* GET stats */
router.get('/',getHome);

/* GET stats */
router.get('/gendevice',getGenDevices);


//ROOMS ROUTES
/* GET rooms. */
router.get('/room', isLoggedIn,asyncErrorHandler(getRooms));

/* CREATE /room */
router.post('/room', isAdult,isLoggedIn,asyncErrorHandler(postNewRoom));

/* GET /rooms form */
router.get('/room/new', isAdult, isLoggedIn, asyncErrorHandler(getRoomsForm));

/* SHOW /room/:id */
router.get('/room/:id', isLoggedIn, showRoomInfo);

/*EDIT /room/:id/edit */
router.get('/room/:id/edit',isAdult, asyncErrorHandler(editRoom));

/*UPDATE /room/:id */
router.put('/room/:id', isAdult,isLoggedIn, asyncErrorHandler(updateRoom));

/* DESTROY /room/:id */
router.delete('/room/:id', isAdult,isLoggedIn, destroyRoom);


//DEVICES ROUTES
/* GET /devices*/
router.get('/room/:id/devices', isLoggedIn, showDevices);

/* GET /room/:id/devices/new */
router.get('/room/:id/devices/new', isAdult, isLoggedIn, asyncErrorHandler(showDeviceForm));

/* CREATE /room/:id/devices */
router.post('/room/:id/devices', isAdult,isLoggedIn, postNewDevice); 

/* EDIT /room/:id/devices/:device_id/edit */
router.get('/room/:id/devices/:device_id/edit', isAdult,isLoggedIn, asyncErrorHandler(editDevice));

/*UPDATE /room/:id/devices/:device_id */
router.put('/room/:id/devices/:device_id', isAdult, isLoggedIn, asyncErrorHandler(updateDevice));

/* DESTROY /room/:id/devices/:device_id */
router.delete('/room/:id/devices/:device_id', isAdult,isLoggedIn, asyncErrorHandler(destroyDevice));

/* UPDATE /room/:id/devices/:device_id */
router.put('/room/:id/devices/:device_id', isAdult,isLoggedIn, asyncErrorHandler(turnDeviceOn));


//USER ROUTES
/* GET /register */
router.get('/register',getRegister);
  
/* POST /register */
router.post('/register',asyncErrorHandler(postRegister));

/* GET /login */
router.get('/login', getLogin);

/* POST/login */
router.post('/login', postLogin);

/*GET /logout */
router.get('/logout', getLogout);


//USER ACCOUNT ROUTES
/* GET /account */
router.get('/account/:id',isLoggedIn, asyncErrorHandler(showMyDetails));

/*EDIT /account/:id/edit */
router.get('/account/:id/edit', isAdult,isLoggedIn,asyncErrorHandler(editMyDetails),
);

/*UPDATE /account/:id/update */
router.put('/account/:id',
 isLoggedIn,
 asyncErrorHandler(isValidPassword),
 asyncErrorHandler(changePassword),
 asyncErrorHandler(updateMyDetails)
);



//HOME MANAGER ROUTES

/* GET /homemanager */
router.get('/homemanager',isLoggedIn,isHomeManager,asyncErrorHandler(getUserDetails));

/*EDIT /homemanager/:id/edit */
router.get('/homemanager/:id/edit',isLoggedIn,isHomeManager,asyncErrorHandler(editUserDetails));

/*UPDATE /homemanager/:id/update */
router.put('/homemanager/:id',isLoggedIn,isHomeManager,asyncErrorHandler(updateUserDetails));

/* DESTROY /homemanager/:id */
router.delete('/homemanager/:id',isLoggedIn,isHomeManager,asyncErrorHandler(deleteUserDetails));

/* GET /homemanagerstats */
router.get('/homemanagerstats',isLoggedIn,isHomeManager,asyncErrorHandler(getHomeManagerStats));

module.exports = router;

