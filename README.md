Taka Smart Home
===

Taka is a smart home system that allows you to view statistics about your home and control smart devices in an easy to use and attractive interface. 

<img src="readmeImages/landingPage.png" width="500" height="250" />
<img src="readmeImages/homePage.png" width="500" height="250" />

### **Prerequisites**
 


- [Node.js](https://nodejs.org/en/) - version 12.14.1 or greater

To check if you have Node.js installed, run this command in your terminal:
```
node -v
 ```

- [npm](https://www.npmjs.com/get-npm) - version 6.13.4 or greater

To confirm that you have npm installed you can run this command in your terminal:
```
npm -v
```

### **Installation**

- To clone this project to your local machine use - `https://gitlab.com/a9800/f29so-project.git`


- Then update the project packages by navigating to `/f29so-project/bockcoewhite/` in your terminal and run:
```
npm install
```

### **Usage**

To build this project: 

1. Navigate to `/f29so-project/bockcoewhite/` in your terminal and then run the following command:
```
npm start
```
2. Then in a web browser open the following URL:

[http://localhost:3000/landing](http://localhost:3000/landing)


Taka Simulator 
===

The simulator for the smart home system that provides the data used for all the values in the home. A generator has also been provided to allow the creation of mock-up data without having to wait for this to propagate naturally.

### **Prerequisites**

- [Python](https://www.python.org/downloads/release/python-381/) - version 3.8.1 or greater

To confirm that you have python installed you can run this command in your terminal:

```
python --version
```

- [PyMongo ](https://pymongo.readthedocs.io/en/stable/) - version 3.10.1 or greater

To confirm that you have PyMongo installed you can run this command in your python terminal:

```
import pymongo
pymongo.version
```
### **Installation**

- To clone this project to your local machine use - `https://gitlab.com/a9800/f29so-project.git`

### **Simulator Usage**

To build the simulator: 

1. Navigate to `/f29so-project/Simulator/` in your python terminal and then run the following command:
```
python SimulatorSingle.py
```

The simulator is now running and generating values for the home.

### **Generator Usage**

To build the simulator: 

1. Navigate to `/f29so-project/Simulator/` in your python terminal and then run the following command:
```
python GeneratorSingle.py
```

2. Enter the number of days to create values for then enter the starting date in the format below (example provided for 4/2/2020):

```
Enter days to run: 7
Enter year: 2020
Enter month: 2
Enter day: 4
```

Values are now generated for this range of date(s). 